
%模型训练师用的俯仰角，但是实际抚养和滚转模式是一样的
y1=myNeuralNetworkFunction(uavEst.pitch_deg);
subplot(2,1,1);
plot(uavTruth.time_s,uavEst.pitch_deg,'r',uavTruth.time_s,y1','--');
legend('原始数据','神经网络跟踪数据')

y1=myNeuralNetworkFunction(uavTruth.roll_deg);
subplot(2,1,2);
plot(uavTruth.time_s,uavEst.roll_deg,'r',uavTruth.time_s,y1','--');
legend('原始数据','神经网络跟踪数据')