function dt=gendt(arg)
%功能：计算轨迹模型的时间步长

npts = max(size(arg(:,1)));
for i=2:npts
    dt(:,i-1) = arg(i,19) - arg(i-1,19);
end

end