function [Psrange_diff,Psrange_diff_Mat]=CALC_Pseudorange(usrpos,svmat)
% 功能： 计算伪距的直线距离和矩阵表达式

NumVis = length(svmat);
Psrange_diff = zeros(NumVis,1);
for N = 1:NumVis
    Psrange_diff(N,1) =  norm(svmat(N,:)-usrpos',2);       %计算伪距
    Psrange_diff_Mat(N,:) = (usrpos'-svmat(N,:))/Psrange_diff(N,1);    
end
end