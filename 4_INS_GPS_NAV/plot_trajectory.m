function plot_trajectory(cfig,profile, posI_only, pos, estenu)
cfig(1); clf;
hold on; grid on;axis([-700 100 -4000 1000 -100 150])
pause(10)
for index = 1:length(profile)
    if mod(index,30)==0
        plot3(profile(1:index,1),profile(1:index,2),profile(1:index,3),'r-.','LineWidth',1);
        plot3(posI_only(1:index,1),posI_only(1:index,2),posI_only(1:index,3),'-')
        plot3(pos(1:index,1),pos(1:index,2),pos(1:index,3),'-','LineWidth',1)
        plot3(estenu(1:index,1),estenu(1:index,2),estenu(1:index,3),'-.')
        drawnow
        F=getframe(gcf);
        I=frame2im(F);
        [I,map]=rgb2ind(I,256);
        if index == 30
            imwrite(I,map,'test.gif','gif', 'Loopcount',inf,'DelayTime',0.2);
        else
            imwrite(I,map,'test.gif','gif','WriteMode','append','DelayTime',0.2);
        end
    end
end

end

