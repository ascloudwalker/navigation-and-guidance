%--------------------------------------------------------------------------
% Algorithm Lab.
% Author：as.ck
% Date：2017年10月13日
% Function: 捷联惯导，导航计算，求解导航方程
% Method：采用INS-GPS深耦合组合导航，间接法，卡尔曼滤波，反馈校正
% Tools：INS工具箱、SatNav3.0工具箱
% Reference：《组合导航技术讲座》、《GPS术语表》、《GPS导航定位原理》
%  《GPS-INS紧耦合组合导航系统研究 刘婧 硕士论文》、 《低成本AHRS/GPS紧耦合融合滤波技术研究》
%--------------------------------------------------------------------------

clear all; close all
%  ========================================================================
% 设定导航的初始位置、初始时间
%  ========================================================================
omega_ie_E =7.292115e-5;                  %地球自转角速率
e          =1/298.257;                    %自然底数
Re         =6378137;                      %赤道地球半径
usrllh = [30.67*pi/180 104.06*pi/180 0];  %设置成都的经纬度
usrxyz = llh2xyz(usrllh);                 %求出ECEF坐标系下的位置,这是地理坐标转换的基准点
usrtime= 0;                               %假设开始导航时间为零，卫星位置就是
                                          %toolbox计算的初始位置，与实际情况
                                          %不符合，不影响方法准确性
loadgps                                   %加载GPS数据模型
mpmat = mpgen(24,3600,1,54321);           %
dph2rps = (pi/180)/3600;                  % 度每小时转弧度每秒

%  ========================================================================
%   首先产生惯性测量数据模型以及GPS接收机测量数据模型，注意是导航坐标系下
%  ========================================================================

%% 1-1 创建运载体的运动模型
fprintf(1,' 创建飞行剖面... \n')
[profile,time]=create_profile();
npts = max(size(time));
true_enu=profile(:,1:3);

for ii=1:npts
    truexyz = enu2xyz(true_enu(ii,:),usrxyz);
    truellh = xyz2llh(truexyz);
    truellh(3) = 0;
    truellw(ii,:) = truellh;                 %将ENU坐标换算成经纬-游动角表示
    DCMel0 = llw2dcm (truellh);
    DCMel_prof(ii,1:3) = DCMel0(1,1:3);
    DCMel_prof(ii,4:6) = DCMel0(2,1:3);
    DCMel_prof(ii,7:9) = DCMel0(3,1:3);
end

%% 1-2 加速度计和陀螺仪漂移模型

%引入加速度漂移（无加速度缩放因子）
n_accel_bias = (100e-6)* gravity(0,0);    % 初始，100微g的漂移
e_accel_bias = (100e-6)* gravity(0,0);
d_accel_bias = (100e-6)* gravity(0,0);
deltime = gendt(profile); % 时间采样矩阵
n_bias = n_accel_bias * deltime;            % 北向由加速度偏差产生
e_bias = e_accel_bias * deltime;            % 东向
d_bias = d_accel_bias * deltime;            % 地向
delv_b0 = [n_bias' e_bias' d_bias'];        % 机体坐标系下的速度偏差

%引入速度误差
n_init_vel_error = 0;                   % 0.1m/s的初始北向误差
e_init_vel_error = 0;
% 初始化地球坐标系到当地坐标系转换
vel_L(1,:) = profile(1,4:6);            % 初始化当地坐标系下的速度估计值
vel2 = vel_L(1,:);  vel1 = vel2;          % 初始化当前和前一时刻的速度
vx1 = vel_L(1,1)+ e_init_vel_error ;       % 初始化东向（x）和北向（y）当前和前
vx2 = vx1;                         % 一时刻的值。
vy1 = vel_L(1,2)+ n_init_vel_error;
vy2 = vy1;
height1 = 0; height2 = 0;   

%引入陀螺仪偏差
n_gyro_bias     = 1*dph2rps;
e_gyro_bias     = 1*dph2rps;
d_gyro_bias     = 1*dph2rps;
acc_n_gyro_bias = n_gyro_bias*deltime;
acc_e_gyro_bias = e_gyro_bias*deltime;
acc_d_gyro_bias = d_gyro_bias*deltime;
dtherr = [acc_n_gyro_bias' acc_e_gyro_bias' acc_d_gyro_bias'];

%引入初始姿态角误差
n_tilt = 0*(10/3600)*pi/180;  % 10 arc-second initial north tilt error
e_tilt = 0;


% 创建速度和加速度模型
fprintf(1,' Creating delta-v and delta-theta profiles \n')
vel_prof_L = profile(:,4:6);             % 速度
DCMnb_prof = profile(:,10:18);           % 导航坐标系到本体坐标系的余弦矩阵
delv_b1    = gendv(vel_prof_L,DCMnb_prof);  % 产生理想的无误差的delta_V,加速度模型
deltheta   = gendthet(DCMnb_prof);         % 产生理想的无误差的delta_Theta，陀螺仪模型
delv_b     = delv_b1 + delv_b0; % 加速度模型添加测量偏差
omega      = deltheta./repmat(deltime',1,3);

% 添加初始姿态误差
DCMnb = [profile(1,10:12); profile(1,13:15); profile(1,16:18)];
DCMbn = DCMnb';
angle = dcm2eulr(DCMbn);
angle(1)=angle(1)*pi/180 + n_tilt;
angle(2)=angle(2)*pi/180 + e_tilt;
angle(3)=angle(3)*pi/180;
DCMnb=eulr2dcm(angle);  DCMbn = DCMnb';
%由地球转动引起的角速率偏差
deltimex=[0 deltime];
deltaer = earthrot(deltimex,DCMel_prof,DCMnb_prof);
est_dtheta =deltheta + deltaer*0 + dtherr*0; % 陀螺仪模型添加偏差量

%%
%%% 初始化中间变量值
daywknum = dayofweek(year(date),month(date),day(date));        %GPS卫星周
gpst=9*60*60+9*60+0;                            %第daywknumz周的第t秒
[svxyzmat0,svid0] = gensv(usrxyz,gpst,5);       %初始时刻观测到的卫星
N = max(size(svid0));                           %观测到的卫星数目
% 可视化当前可观测卫星
% figure
% skyplot(svxyzmat0,svid0,usrxyz,0,1)
% hold on
% str1=sprintf...
% ('地点：成都（北纬30.67度，东经104.06度）\n 时间：北京时间9时9分0秒 ');
% title(str1)

%%% 初始值、中间值
pos(1,1:3) = profile(1,1:3);posI=pos; estenu = pos;
[prvec_0,adrvec_0] = genrng(1,usrxyz,svxyzmat0,svid0,gpst,[1 1 0 1 1],[],mpmat);
estusr_0   = olspos(prvec_0, svxyzmat0);
enu_err    = (xyz2enu(estusr_0(1:3), usrxyz))';
prvecusr_0 = prvec_0';
t=gpst;
%%%  卡尔曼模型系统参数值
% WI=[wgx wgy wgz wzx way waz]';                        %系统噪声向量
% WG=[wtu -wtru]';                                      %测量噪声向量
Betatru=0.001;
P_last=zeros(17,17);
Q0=1e-3*eye(8,8);
R0=1e-8*eye(16,16);

%初始的状态量是直接取的真值，所以偏差都是零。
phi           = zeros(1,3) ; % 平台误差角
delta_v       = zeros(1,3) ;
delta_p       = zeros(1,3) ;
delta_epsilon = zeros(1,3) ;
delta_chi     = zeros(1,3) ;
delta_tu      = 0; % 时钟偏置等效距离误差
delta_tru     = 0; % 钟飘等效距离率误差
X0            = [phi delta_v delta_p delta_epsilon delta_chi delta_tu delta_tru]'+1e-4;

C = [0 1 0; 1 0 0; 0 0 -1];                        % 将北东地系转化为东北天系
Cnc=[1  phi(3)  -phi(2);-phi(3) 1 phi(1);phi(2)  -phi(3) 1]  ;

tru_lat = 30.67*pi/180; tru_long = 104.06*pi/180; tru_alpha = 0;
est_lat = tru_lat;est_long = tru_long;est_alpha = tru_alpha;est_h = 0;
lat1 = est_lat;lat2 = est_lat;            % 初始化当前（2）和前一时刻（1）的值
X_last = X0; 
prvecusr_ins_last =prvecusr_0;prvecusr_gps_last =prvecusr_0;
velocity = profile(1,4:6);
attitude_angle = [0,0,0];
DCMel = llw2dcm([tru_lat tru_long tru_alpha]);
DCMbn_only = DCMbn;vel_L_only = vel_L;posI_only = pos;
%%
% ----------------------------------------------------------------------------------------
fprintf(1,' 开始仿真... \n')
state_bar = waitbar(0,'INS/GPS组合导航计算...','Name','导航状态');      % 创建仿真进度条
for ii=2:npts
    td12=deltime(ii-1);
    tdex = 0.5*td12;
    tdint = td12;                                   %更新每时每刻加速度计测量
    g = gravity(tru_lat,height2);                   %飞行器位置的铅锤重力
    index = ii-1;
%%
%-----------------------------------------------------------------------
%  惯性系统输出
%——————————————————————————————————— 
    gv=DCMbn'*[0,0,-g]';  %重力加速度值分量
    
%     % 先计算纯惯性导航
    DCMbn_only = bodupdat(DCMbn_only,est_dtheta(index ,:));
    del_VL_only   = C * (DCMbn_only * (delv_b(index,:)'));
    vel_L_only(ii,:) = vel_L_only(ii-1,:) + del_VL_only';
    posI_only(ii,1:3)= posI_only(ii-1,1:3) + td12 * mean([vel_L_only(ii,1:3); vel_L_only(ii-1,1:3)]);
    
    % 引入修正
%     delta_chi =-delv_b0(index,:);
    delv_b(index,:)= delv_b(index ,:) + delta_chi; % 加速度模型修正
    est_dtheta(index,:)= est_dtheta(index ,:) + delta_epsilon; % 陀螺仪模型修正
    Cnc=[1   phi(3)  -phi(2);-phi(3)   1  phi(1);phi(2)  -phi(3)  1];
    DCMbn = bodupdat(DCMbn,est_dtheta(index ,:)); 
%     DCMbn =  DCMbn * Cnc ; % 修正平台误差角

    del_VL   = C * (DCMbn * delv_b(ii-1,:)');
    vel_L(ii,:) = vel_L(ii-1,:) + del_VL'; % update local-level velocity
  
    %惯性传感器的观测位置
    posI(ii,1:3)= posI(ii-1,1:3) + td12 * mean([vel_L(ii,1:3); vel_L(ii-1,1:3)]);
    llw_vect = dcm2llw(DCMel); % 方向余弦矩阵到经纬度

%%
%----------------------------------------------------------------------------
% GPS系统输出
%——————————————————————————————————————
    t               = t+td12;
    usrxyzpath      = enu2xyz(profile(ii-1,1:3),usrxyz); %东北天 转 WGS-84
    [svxyzmat,svid] = gensv(usrxyzpath,t,5); %创建可见卫星的位置矩阵
    %计算测量伪距以及所有可见卫星的累积距离差值
    [prvec,adrvec]  = genrng(1,usrxyzpath,svxyzmat,svid,t,[1 1 0 1 1],[],mpmat); % 真实伪距
    estusr          = olspos(prvec,svxyzmat);%最小二乘法,用卫星位置和伪距计算位置
    estenu(ii,:)    = (xyz2enu(estusr(1:3), usrxyz) )'; % WGS-84 转 东北天  
    %%
%---------------------------------------------------------------------------
%          组合导航的常规卡尔曼滤波器状态方程和量测方程,用上一状态
%——————————————————————————————————————
    h=pos(ii-1,3);
    ve=vel_L(ii-1,1); vn=vel_L(ii-1,2); vu=vel_L(ii-1,3);
    Lat=est_lat(ii-1);                                        %计算的纬度值
    Lon= est_long(ii-1);                                      %计算的经度值
    Rn=Re*(1+e*(sin(Lat))^2);
    Rm=Re*(1-2*e+3*e*(sin(Lat))^2);
    Rnh=1/(Rn+h);Rmh=1/(Rm+h);
    f=DCMbn * delv_b(ii-1,:)' / td12;                             %导航坐标系比力
    fe=f(1);fn=f(2);fu=f(3);
    
    FN=zeros(9,9);
    
    FN(1,2)=omega_ie_E * sin(Lat)+ve*Rnh*tan(Lat);
    FN(1,3)=-(omega_ie_E*cos(Lat)+ve*Rnh);
    FN(1,5)=-(Rmh);
    FN(2,1)=-(omega_ie_E*sin(Lat)+ve*Rnh*tan(Lat));
    FN(2,3)=-vn*Rmh;
    FN(2,4)=Rnh;
    FN(2,7)=-omega_ie_E*sin(Lat);
    FN(3,1)=omega_ie_E*cos(Lat)+ve*Rnh;
    FN(3,2)=vn*Rmh;
    FN(3,4)=tan(Lat)*Rnh;
    FN(3,7)=omega_ie_E*cos(Lat)+ve*Rnh*(sec(Lat))^2;
    FN(4,2)=-fu;
    FN(4,3)=fu;
    FN(4,4)=vn*Rmh*tan(Lat)-vu*Rmh;
    FN(4,5)=2*omega_ie_E*sin(Lat)+ve*Rnh*tan(Lat);
    FN(4,6)=-(2*omega_ie_E*cos(Lat)+ve*Rnh);
    FN(4,7)=2*omega_ie_E*cos(Lat)*vn+ve*vn*Rnh*(sec(Lat))^2+2*omega_ie_E*sin(Lat)*vu;
    FN(5,1)=fu; FN(5,3)=-fe;
    FN(5,4)=-2*(omega_ie_E*sin(Lat)+ve*Rnh*tan(Lat));
    FN(5,5)=-vu*Rmh;
    FN(5,6)=-vn*Rmh;
    FN(5,7)=-(2*omega_ie_E*cos(Lat)+ve*Rnh*(sec(Lat))^2)*ve;
    FN(6,1)=-fn; FN(6,2)=-fe;
    FN(6,4)=2*(omega_ie_E*cos(Lat)+ve*Rnh);
    FN(6,5)=2*vn*Rmh;
    FN(6,7)=-2*omega_ie_E*sin(Lat)*ve;
    FN(7,5)=Rmh;
    FN(8,4)=sec(Lat)*Rnh;
    FN(8,7)=ve*Rnh*sec(Lat)*tan(Lat);
    FN(9,6)=1;
    
    FS=-[DCMbn,zeros(3,3);zeros(3,3),DCMbn;zeros(3,6)];
    FM=zeros(6,6);
    %  GI=[DCMbn,zeros(3,3);zeros(3,3),DCMbn;zeros(9,6)];
    
    %系统状态方程矩阵
    FI=[FN,FS;zeros(6,9),FM];  % INS 状态转移矩阵
    GI=[DCMbn,zeros(3,3);zeros(3,3),DCMbn;zeros(9,6)];
    FG=[1 0; 0 -Betatru];  % GPS状态转移矩阵
    GG=[1 0;0 1];
    
    F=[FI zeros(15,2); zeros(2,15) FG];
    G=[GI zeros(15,2); zeros(2,6)  GG];
    
    %%% 计算观测量
    % 惯导伪距，相当于系统输出
    usrxyzpath_ins  = enu2xyz(posI(ii,:),usrxyz);
    [svxyzmat_ins,svid_ins] = gensv(usrxyzpath_ins,t,5);
    [~,prvecusr_ins_mat] = CALC_Pseudorange(usrxyzpath_ins,svxyzmat_ins(1:8,:));  %最优估计值伪距
    [PsRange_ins,~]  = genrng(1,usrxyzpath_ins,svxyzmat_ins(1:8,:),svid,t,[1 1 0 1 1],[],mpmat); % 真实伪距
    prvecusr_inst_rate =(PsRange_ins' - prvecusr_ins_last) / td12;
    prvecusr_ins_last = PsRange_ins';
    % GPS伪距 相当于卫星接收机
    usrxyzpath_gps  = enu2xyz(profile(ii,1:3),usrxyz); % 真值
    % 利用惯导短期内精度高，相当于真值，计算伪距和伪距率误差，再用卡尔曼方程估计值反回来修正惯导
%     usrxyzpath_gps = usrxyzpath_ins; 
    [svxyzmat_gps,svid_gps] = gensv(usrxyzpath_gps,t,5);
    [~,prvecusr_gps_mat] = CALC_Pseudorange(usrxyzpath_gps,svxyzmat_gps(1:8,:));  %最优估计值伪距
    [PsRange_gps,~]  = genrng(1,usrxyzpath_gps,svxyzmat_ins(1:8,:),svid,t,[1 1 0 1 1],[],mpmat); % 真实伪距
    prvecusr_gps_rate = (PsRange_gps' - prvecusr_gps_last) / td12;
    prvecusr_gps_last = PsRange_gps';
    
    Z_k = [PsRange_ins'-PsRange_gps'; prvecusr_inst_rate-prvecusr_gps_rate]; %当前观测量
    
    %    Rn=1;
    diff_v =  prvecusr_ins_mat;
    diff_v_rate = prvecusr_inst_rate - prvecusr_gps_last;
    Lon1= llw_vect(2); Lat1= llw_vect(1);
%      Lon1= est_long(ii-1); Lat1= est_lat(ii-1);
%     satnum = length(svid_ins);
    satnum = 8;
    Hrho1=zeros(satnum,3);  % SINS伪距量测方程
    for k=1:satnum
        Hrho1(k,1)=(Rn+h)*(-diff_v(k,1)*sin(Lat1)*cos(Lon1)-diff_v(k,2)*sin(Lat1)...
            *sin(Lon1))+(Rn*(1-e^2)+h)*diff_v(k,3)*cos(Lat1);
        Hrho1(k,2)=(Rn+h)*(diff_v(k,2)*cos(Lat1)*cos(Lon1)-diff_v(k,1)*cos(Lat1)...
            *sin(Lon1));
        Hrho1(k,3)=diff_v(k,1)*cos(Lat1)*cos(Lon1)+diff_v(k,2)*cos(Lat1)*sin(Lon1)+...
            diff_v(k,3)*sin(Lat1);
    end
    Hrho2=[ones(satnum,1) zeros(satnum,1)]; % GPS伪距测方程
    
    
    Hrho1_dot=zeros(satnum,3);% SINS伪距差量测方程
    for k=1:satnum
        Hrho1_dot(k,1)=-diff_v(k,1)*sin(Lon1)+diff_v(k,2)*cos(Lon1);
        Hrho1_dot(k,2)=-diff_v(k,1)*sin(Lat1)*cos(Lon1)-diff_v(k,2)*...
            sin(Lat1)*sin(Lon1)+diff_v(k,3)*cos(Lat1);
        Hrho1_dot(k,3)=diff_v(k,1)*cos(Lat1)*cos(Lon1)+diff_v(k,2)*...
            cos(Lat1)*sin(Lon1)+diff_v(k,3)*sin(Lat1);
    end
    Hrho2_dot=zeros(satnum,3); % GPS伪距量测方程

    
    Da(1,1)=-(Rn+h)*sin(Lat1)*cos(Lon1);
    Da(1,2)=-(Rn+h)*cos(Lat1)*sin(Lon1);
    Da(1,3)=cos(Lat1)*cos(Lon1);
    Da(2,1)=-(Rn+h)*sin(Lat1)*sin(Lon1);
    Da(2,2)=(Rn+h)*cos(Lat1)*cos(Lon1);
    Da(2,3)=cos(Lat1)*sin(Lon1);
    Da(3,1)=(Rn*(1-e^2)+h)*cos(Lat1);
    Da(3,2)=0;
    Da(3,3)=sin(Lat1);
    
    ve1=vel_L(ii-1,1); vn1=vel_L(ii-1,2); vu1=vel_L(ii-1,3); %当前速度值
    
    De(1,1)=-vn1*cos(Lat1)*cos(Lon1)-vu1*sin(Lat1)*cos(Lon1);
    De(1,2)=-ve1*cos(Lon1)+vn1*sin(Lat1)*sin(Lon1)-vu1*cos(Lat1)*sin(Lon1);
    De(1,3)=0;
    De(2,1)=-vn1*cos(Lat1)*sin(Lon1)-vu1*sin(Lat1)*sin(Lon1);
    De(2,2)=-ve1*sin(Lon1)-vn1*sin(Lat1)*cos(Lon1)+vu1*cos(Lat1)*cos(Lon1);
    De(2,3)=0;
    De(3,1)=-vn1*sin(Lat1)+vu1*cos(Lat1);
    De(3,2)=0;
    De(3,3)=0;
    
    %Hrho2_dot=erro*De + grr*Da;
    Hrho3_dot=[zeros(satnum,1) ones(satnum,1)];
    H_k=[zeros(satnum,6) Hrho1                  zeros(satnum,6) Hrho2;
         zeros(satnum,3) Hrho1_dot Hrho2_dot    zeros(satnum,6) Hrho3_dot];
    
    %连续系统方程的离散化计算以及卡尔曼滤波器公式，0表示上一步，1表示当前
    F_k= eye(17,17) +  F;
    G10 = G;
      
%    X_new  = F_k * X_last;                               % 一步估计
    X_new  = (eye(size(F_k))+ 0.00001*F_k * td12 ) * X_last;               % 一步估计
    P_new  = F_k * P_last * F_k'+ G10 * Q0 * G10';             % 协方差矩阵
    K_new  = P_new * H_k' / (H_k * P_new * H_k'+ R0);          % 滤波器增益
    X_new  = X_new + K_new * ( Z_k - H_k * X_new);             % 计算最优估计值
    P_new  = (eye(17,17)-K_new * H_k) * P_new;               % 更新协方差矩阵
   
    %%
    %----------------------------------------------------------------------
    %        用偏差值补偿加速度计、陀螺仪、位置、速度、平台角、GPS
    %        X0 = [phi delta_v delta_p delta_epsilon delta_chi deltatu deltaru ]';
    %——————————————————————————————————————
    phi           = -X_new(1:3)' ;
    delta_v       = -X_new(4:6)' ;
    delta_p       = -X_new(7:9)  ;
    delta_epsilon = -X_new(10:12)' ;
    delta_chi     = -X_new(13:15)' ;
    
    est_lat(ii)   = llw_vect(1)+ delta_p(1) ;
    est_long(ii)  = llw_vect(2)+ delta_p(2) ;
    vel_L(ii,:)   = vel_L(ii,:)+ delta_v * td12;

    pos(ii,1:3)= pos(ii-1,1:3) + td12 * mean([vel_L(ii,1:3); vel_L(ii-1,1:3)]);

    % 循环数据更新
    vx1  = vx2; vx2  = vel_L(ii,1);         
    vy1  = vy2; vy2  = vel_L(ii,2);
    vel1 = vel2; vel2 = vel_L(ii,:);
    lat1 = lat2; lat2 =  est_lat(ii);
    height1= height2;height2 = pos(ii,3);
    X_last = X_new; P_last = P_new; % 更新
    
    % 记录数据
%     state(:,ii-1) = X_new;
    waitbar(ii/npts);                              %显示仿真运行进度
end
close(state_bar);
%%
%画出本地坐标系下导航轨迹
figure;
plot3(profile(:,1),profile(:,2),profile(:,3),'r-.','LineWidth',1);hold on
plot3(posI_only(:,1),posI_only(:,2),posI_only(:,3),'-o')
plot3(pos(:,1),pos(:,2),pos(:,3),'-','LineWidth',1)
plot3(estenu(:,1),estenu(:,2),estenu(:,3),'-.')

title('INS/GPS紧耦合组合导航飞行轨迹')
xlabel('东 (meters)')
ylabel('北 (meters)')
zlabel('天 (meters)')
legend('标称轨迹','纯惯性导航','组合导航','卫星导航轨迹')
grid on
grid minor
% axis equal

figure
title_txt=["东向位置估计误差","北向位置估计误差","天向位置估计误差"];
for j = 1:3
    subplot(3,1,j)
    plot(pos(:,j)-profile(:,j),'r','LineWidth',2)
    grid on
    grid minor
    title(title_txt(j));
    xlabel('None');
    ylabel('米');
end

cfig = figure('Name','AS.CK INS/GPS','NumberTitle','off');
plot_trajectory(cfig,profile, posI_only, pos, estenu)