function grr=cacugrr(v,usrpos,svxyzmat,svxyzmat0,td12,rho_dot,erro)
%--------------------------------------------------------------------------
% 功能：计算观测方程的g
numvis = max(size(svxyzmat));
z=zeros(numvis,1);
for N = 1:numvis
    z = norm(svxyzmat(N,:)-usrpos');       %计算伪距
    vs=(svxyzmat(N,:)-svxyzmat0(N,:))/td12;
    grr(N,:)=(v-vs-rho_dot(N)*erro(N,:))/z;
end
end