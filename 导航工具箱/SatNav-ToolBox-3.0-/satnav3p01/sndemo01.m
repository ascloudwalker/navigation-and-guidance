%  sndemo01.m       Example of satellite position generation and plotting
clear all
close all
%    
t = 39600;
usrllh = [30.67*pi/180 104.06*pi/180 0];%γ�ȡ�����
usrxyz = llh2xyz(usrllh);
loadgps
[svxyzmat,svid] = gensv(usrxyz,t,5);
skyplot(svxyzmat,svid,usrxyz)
text(0.5,0.9,'Simulated GPS Constellation')
text(0.5,0.8,'User at Lat 30.67 deg, Lon 104.06 deg')
