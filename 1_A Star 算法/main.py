# Algorithm Studio
# Editor：CloudwalKer
# main.py
# Date:---
# Function:主程序入口

# 第三方库
import numpy as np
import matplotlib.pyplot as plt 
from matplotlib.patches import Rectangle

# 自定义库
import random_map
import a_star

# plt.figure(figsize=(5, 5))

map = random_map.RandomMap()

#创建绘图用的坐标轴
ax = plt.gca()
ax.set_xlim([0, 1500])
ax.set_ylim([0, 500])

for node in map.map.G.nodes():

    x = node.x
    y = node.y

    # 如果是障碍点，就显示为灰色格点，如果不是，就显示为白色
    if map.IsObstacle(x,y):
        rec = Rectangle((x,y), width=1, height=1, color='gray')
        ax.add_patch(rec)
    else:
        rec = Rectangle((x,y), width=10, height=10, edgecolor='gray', facecolor='w')
        ax.add_patch(rec)

rec = Rectangle((0, 0), width = 1, height = 1, facecolor='b')
ax.add_patch(rec)

# rec = Rectangle((map.size-1, map.size-1), width = 1, height = 1, facecolor='r')
# ax.add_patch(rec)

plt.axis('equal')
plt.axis('off')
plt.tight_layout()
# plt.show()

a_star = a_star.AStar(map)
a_star.RunAndSaveImage(ax, plt)