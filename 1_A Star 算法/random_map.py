# random_map.py

import numpy as np

import point
import nodeNet

class RandomMap:
    def __init__(self, size=50):
        self.obstacle = size//8
        self.GenerateObstacle()
        self.map = nodeNet.graph()
        pass


    # 创建障碍点，可以根据自己的需求更改，将定义好的nodeNet部分数据添加在此（本实例中没有添加）
    def GenerateObstacle(self):
        obstaclepoint=np.array([9, 12, 12, 12, 13, 12])
        obstaclepointMatrix = obstaclepoint.reshape(3,2)
        self.obstacle_point = []
        for row in obstaclepointMatrix:
            self.obstacle_point.append(point.Point(row[0],row[1]))
        pass


        # Generate an obstacle in the middle
        # for i in range(self.size//2-4, self.size//2):
        #     self.obstacle_point.append(point.Point(i, self.size-i))
        #     self.obstacle_point.append(point.Point(i, self.size-i-1))
        #     self.obstacle_point.append(point.Point(self.size-i, i))
        #     self.obstacle_point.append(point.Point(self.size-i, i-1))

        # for i in range(self.obstacle-1):
        #     x = np.random.randint(0, self.size)
        #     y = np.random.randint(0, self.size)
        #     self.obstacle_point.append(point.Point(x, y))

        #     if (np.random.rand() > 0.5): # Random boolean
        #         for l in range(self.size//4):
        #             self.obstacle_point.append(point.Point(x, y+l))
        #             pass
        #     else:
        #         for l in range(self.size//4):
        #             self.obstacle_point.append(point.Point(x+l, y))
        #             pass

    def IsObstacle(self, i ,j):
        # for p in self.obstacle_point:
        #     if i==p.x and j==p.y:
        #         return True
        # return False

        # 判断是否是障碍点，这个规则需要自己写。
        # 
        if i>=self.obstacle_point[0].x and i<=self.obstacle_point[2].x and j>=self.obstacle_point[0].y and j<=self.obstacle_point[2].y  or i==0 or j==0:
            return True
        else:
            return False