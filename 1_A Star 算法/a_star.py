# a_star.py
# A*算法的计算过程
import sys
import time

import numpy as np

from matplotlib.patches import Rectangle

import point
import random_map

class AStar:
    def __init__(self, map):
        self.map=map
        self.open_set = []
        self.close_set = []

    def BaseCost(self, p, startPoint):
        x_dis = abs(startPoint.x- p.x)
        y_dis = abs(startPoint.x- p.x)
        # Distance to start point
        return  1*(x_dis + y_dis) 

    def HeuristicCost(self, p, endPoint):
        x_dis = abs(endPoint.x- p.x)
        y_dis = abs(endPoint.y- p.y)
        # Distance to end point
        return  1*(x_dis + y_dis) 

    def TotalCost(self, p,startPoint,endPoint):
        return self.BaseCost(p,startPoint) + self.HeuristicCost(p,endPoint)

    def IsValidPoint(self,m,n):
        if m < 0 or n < 0:
            return False
        if m >=4 or n >=8 :
            return False
        x = self.map.map[m,n,0]
        y = self.map.map[m,n,1]
        return not self.map.IsObstacle(x, y)

    def IsInPointList(self, p, point_list):
        for point in point_list:
            if point.x == p.x and point.y == p.y:
                return True
        return False

    def IsInOpenList(self, p):
        return self.IsInPointList(p, self.open_set)

    def IsInCloseList(self, p):
        return self.IsInPointList(p, self.close_set)

    def IsStartPoint(self, p):
        return p.x == self.map.map.pointlist[1].x and p.y == self.map.map.pointlist[1].y

    def IsEndPoint(self, p):
        return p.x == self.map.map.pointlist[4].x and p.y == self.map.map.pointlist[4].y

    def SaveImage(self, plt):
        millis = int(round(time.time() * 1000))
        filename = './' + str(millis) + '.png'
        plt.savefig(filename)

    def ProcessPoint(self, node, parent, startPoint, endPoint):
      
        # if not self.IsValidPoint( m , n):
        #     return # Do nothing for invalid point
        # 如图在closelist中，就不做操作，否则，就添加在开集中，并且要指定它的上一个连接点（parent）
        p = node
        if self.IsInCloseList(p):
            return # Do nothing for visited point
        print('Process Point [', p.x, ',', p.y, ']', ', cost: ', p.cost)
        if not self.IsInOpenList(p):
            p.parent = parent
            p.cost = self.TotalCost(p,startPoint,endPoint)
            self.open_set.append(p)

    def SelectPointInOpenList(self,startPoint,endPoint):
        index = 0
        selected_index = -1
        # min_cost = sys.maxsize
        min_cost = 1700
        for p in self.open_set:
            cost = self.TotalCost(p,startPoint,endPoint)
            if cost < min_cost:
                min_cost = cost
                selected_index = index
            index += 1
        return selected_index

    def BuildPath(self, p, ax, plt, start_time):
        path = []
        while True:
            path.insert(0, p) # Insert first
            if self.IsStartPoint(p):
                break
            else:
                p = p.parent
        for p in path:
            rec = Rectangle((p.x, p.y), 10, 10, color='g')
            ax.add_patch(rec)
            plt.draw()
            self.SaveImage(plt)
        end_time = time.time()
        print('===== Algorithm finish in', int(end_time-start_time), ' seconds')

    # A* 算法的计算入口
    def RunAndSaveImage(self, ax, plt):
        start_time = time.time()

        startPoint = self.map.map.pointlist[1] # 指定路径起点
        startPoint.cost = 0
        self.open_set.append(startPoint)

        endPoint = self.map.map.pointlist[4] # 指定路径的终点

        while True:
            
            # 在openlist中依次选点计算
            index = self.SelectPointInOpenList(startPoint,endPoint)

            if index < 0:
                print('No path found, algorithm failed!!!')
                return

            # 绘图并保存
            p = self.open_set[index]
            rec = Rectangle((p.x, p.y), 10, 10, color='c')
            ax.add_patch(rec)
            self.SaveImage(plt)

            # 如果是终点，就绘图保存并推出
            if self.IsEndPoint(p):
                return self.BuildPath(p, ax, plt, start_time)

            # 已经使用的点不能再使用，所以从openlist中删除，加入道clsoelist当中
            del self.open_set[index]
            self.close_set.append(p)

            # 处理当前点的相邻点
            for neiborNode in self.map.map.G.neighbors(p):
                self.ProcessPoint(neiborNode, p, startPoint,endPoint)





