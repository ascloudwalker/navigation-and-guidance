# Funciton: 将实际的问题转化为数学问题，建立拓扑图

import matplotlib.pyplot as plt
import networkx as nx
import point
import math
import numpy as  np
import scipy as np

class graph():
    def __init__(self):
        self.G=nx.Graph()
        # x 坐标；y 坐标；使用经纬度时，可以转换为直角坐标使用。
        x = np.array( [45.00000,1231.69098,1131.32004,1131.14803,1130.61881,1174.52086,1175.06277,1203.83593]) 
        y = np.array([ 218.50000,41.07903,0.86073,42.51475,89.66572,40.89967, 23.54060,41.22488])

        # 将x,y坐标组合成为一个point类，方便使用
        self.pointlist = []
        for x1,y1 in zip(x,y):
            p = point.Point(x1,y1)
            self.pointlist.append(p)
        del p,x1,y1

        # 除以10再取整，我这么做的原因：我实际测量的数据之间有偏差，所以我认为坐标差值<10的是相邻且有连接的点。 
        a = np.trunc(x/10)
        b = np.trunc(y/10)

        # 删除重复元素
        aa = set(a)
        bb = set(b)
        
        # 思路：先找到相同的point.x值point，按照point.y值排列，这些point就会按照排列顺序纵向连接
        # 再找到相同的point.y值point，按照point.x值排列，这些point就会按照排列顺序横向连接
        for elex,eley in zip(aa,bb):
            indexx = np.argwhere( a == elex) # 选出所有x坐标相同的点的序号值
            bslice = b[indexx] # 获得所有x坐标相同点的y坐标值
            sortnumb = np.argsort([i for item in bslice.tolist() for i in item]) # 按照y值进行排序，并获得序号值
            indexxsort = indexx[sortnumb] # 获得x值按照y值大小排序后的位置值
            indexy = np.argwhere( b == eley) # 以下同上
            aslice = a[indexy]
            sortnuma = np.argsort([i for item in aslice.tolist() for i in item])
            indexysort = indexy[sortnuma]

            # 如果相同x和相同y的数目都是1，那个该点是个孤立点
            if len(indexxsort)==1 and len(indexysort)==1: 
                self.G.add_node(self.pointlist[indexxsort[0][0]])  

            # 如果相同x数目大于1，那么这些点有连接，添加连接线，也就是edge
            if len(indexxsort)>1:
                for  i in range(0,len(indexxsort)-1):
                    self.G.add_edge(self.pointlist[indexxsort[i][0]],self.pointlist[indexxsort[i+1][0]])  
            
            # 如果相同y数目大于1，那么这些点有连接，添加连接线，也就是edge
            if len(indexysort) >1:
                for  i in range(0,len(indexysort)-1):
                    self.G.add_edge(self.pointlist[indexysort[i][0]],self.pointlist[indexysort[i+1][0]]) 

        # nx.algorithms.bipartite.matrix.biadjacency_matrix(G,G.nodes())
        # Adj=nx.adjacency_matrix(self.G)
        # print(Adj.todense())
        nx.draw(self.G,with_labels=True)
        [n for n in self.G.neighbors(self.pointlist[5])]
        plt.show()
