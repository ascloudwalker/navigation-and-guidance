% Algrithom Lab.
% Author:as.ck
% Date:2021-3
% Function: 路径导引
% 参考：北航多旋翼课程课件 13讲
%       《Vector Field Path Following for Miniature Air Vehicles》
%       《A Guiding Vector-Field Algorithm for Path-Following Control of Nonholonomic》

%%
clear
%%% 质点模型
u_0 = [0 0];v_0 = [.2,.2];p_0 = [0,-4];
p_0 = [0,20];p_target = [0,10];
ts=0.2;

% 导引跟踪过程结算
v_last = v_0;p_last = p_0;
ks = 10 ; % 改变该参数，跟踪轨迹会变化
for i=1:100/ts
   
    x=p_last(1);y=p_last(2);
    E = [0 1;-1 0];
    error = atan(x - y); % 求误差
    tau_ver = E * [1,-1]'; % 求法向量
    vf = -tau_ver - ks * error * [1,-1]'; % 向量场
    vf = vf / norm(vf,2); % 归一化
    
    % 运动学方程
    u = vf';
    vel = norm(v_0,2)*u;
    p = p_last + vel * ts;
    
    p_last = p;
    v_last = vel;
    position(i,:) = p;
end

%%
% 直线势场 y = x;
[X,Y] = meshgrid(0:.4:12);
error = atan(X-Y); % 偏差
for i =1: length(X)
    for j =1: length(Y)
        tau = [1,1] - ks* atan(error(i,j)).*[1,-1];  % 法向量-ks*e*切向量
        tau = tau / norm(tau,2); % 归一化
        u(i,j) = tau(1);
        v(i,j) = tau(2);
    end
end

% 绘制向量场和路径
quiver(X,Y,u,v,'color',[.5 .5 .5],'LineWidth',0.5)
hold on
plot([1,12],[1,12],'LineWidth',3)
plot(position(:,1),position(:,2),'b','LineWidth',2)
plot(p_0(1),p_0(2),'Marker','o','MarkerFaceColor','red','MarkerSize',10)
grid on
grid minor
title('人工势场/向量场导引')
xlabel("x/m");ylabel("y/m")



% %调整显示范围
% h = figure(1);
% h.Children.XLim=[0,12];
% h.Children.YLim=[0,12];

legend('势场','目标路径','跟踪路径','初始位置')
