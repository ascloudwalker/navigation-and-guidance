
%%
clear
close all
%%% �ʵ�ģ��
u_0      = [0 0];
v_0      = [-20,-20];
p_last   = [0,-200];
p_0      = [0,0];
p_target = [400, -400];
ts=0.2;

% ��������
chi_inf = pi/2;
alpha   = 1.1;
Vg      = norm(v_0,2);
chi_bar = 1;
k       = .01;
kai     = 1;

%
vel_v       = v_0;
chi_err = 0;
for i =1:10/ts

    % �����ƫ
    x=p_last(1);y=p_last(2);
    cur_2_next  = p_target - [x, y];
    pre_2_next  = p_target -  p_0;
    pre_2_next  = pre_2_next / norm(pre_2_next,2); % ��һ��
    cross_err_v = cross([cur_2_next,0],[pre_2_next,0]);
    cross_err(i)   = cross_err_v(3); % ��ƫ����

    % ������Ŀ�꺽���Ǽ���
    cur_bearing = wrapTo2Pi(atan2(vel_v(1),   vel_v(2)));
    tar_bearing = wrapTo2Pi(atan2(p_target(1),p_target(2)));
    
    % �����뺽�߼н�
    chi         = wrapToPi(cur_bearing - tar_bearing)  ;

    % ���������ǣ��켣�滮������
    chi_d(i)    =  wrapToPi(-chi_inf * 2 / pi *  atan(k * - cross_err(i))); 
    chi_bar     = wrapToPi(chi_err - chi_d(i));

    %������ָ��켣���ٺ�����
    chi_c(i)    =  tar_bearing -  1/ alpha * chi_inf * 2 / pi  * k /(1 + k^2 * cross_err(i)^2) * Vg *sin(chi_err) -  kai / alpha * sat(chi_bar);
    
    chi_dot     = wrapToPi(chi_c(i) - cur_bearing);
    roll_deg(i)    = 0.2 * atan(chi_dot * Vg / 9.81) * 57.3;
    
    if i*ts == 15
        pp = 1;
    end
    % �˶�ѧ����
%     vel_v = [sin(chi_d(i)+ tar_bearing), cos(chi_d(i) + tar_bearing)];
    vel_v = [sin(chi_c(i)), cos(chi_c(i) )];
    vel   = norm(v_0,2)*vel_v;
    p     = p_last + vel * ts;
    
    chi_cur(i)    = cur_bearing;
    p_last        = p;
    v_last        = vel;
    position(i,:) = p;
    time(i)       = i*ts;
end  
% figure
% plot(time, chi_d*57.3)
% hold on
% plot(time, chi_cur,'r-')
figure
plot(position(:,1), position(:,2))
hold on
plot([p_0(1), p_target(1)], [p_0(2), p_target(2)])
% figure
% plot(time, cross_err);

function y = sat(x)
    if abs(x) <= 1
        y = x;
    else
        y = sign(x);
    end
end