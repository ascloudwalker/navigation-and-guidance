% Algorithm Lab.
% Author:as.ck
% Date:2021-3-20
% Function:创建支线和椭圆的人工势场/向量场

close all
clear all
%%
% 直线 y = x;
[X,Y] = meshgrid(0:.4:12);
error = atan(X-Y); % 偏差
for i =1: length(X)
    for j =1: length(Y)
        tau = [1,1] - 1* error(i,j).*[1,-1];  % 法向量-ks*e*切向量
        tau = tau / norm(tau,2); % 归一化
        u(i,j) = tau(1);
        v(i,j) = tau(2);
    end
end


% 绘制向量场
quiver(X,Y,u,v)
hold on
plot([1,12],[1,12],'LineWidth',2)
grid on
grid minor

%调整显示范围
h = figure(1);
h.Children.XLim=[0,12];
h.Children.YLim=[0,12];

%%
%椭圆向量场
clear u v
figure
[X,Y] = meshgrid(30:1:70);
x0 = 50; y0 = 50;
a = 15; b=5;
n_ver_1 = 2*(X-x0) / a^2; %对x偏导数 
n_ver_2 = 2*(Y-x0) / a^2; %对y偏导数
for i =1: length(X)
    for j =1: length(Y)
        E = [0 1;-1 0];
        error = (X(i,j)-50)^2/15^2 + (Y(i,j)-50)^2/10^2 -1; % 求误差
        tau_ver = E * [n_ver_1(i,j),n_ver_2(i,j)]'; % 求法向量
        vf = tau_ver - 1 * atan(error) * [n_ver_1(i,j),n_ver_2(i,j)]'; % 向量场
        vf = vf / norm(vf,2); % 归一化
        u(i,j) = vf(1);
        v(i,j) = vf(2);
    end
end

% 绘制向量场
quiver(X,Y,u,v)
hold on
ezplot('(x-50)^2/15^2 + (y-50)^2/10^2 = 1',[30 70 30 70])
